<?php
  class Frog extends animal {
    public $name;
    public $legs = 4;
    public function __construct($name) {
      $this->name = $name;
      $this->cold_blooded = "no";
    }

    public function jump() {
      echo "Hop Hop";
    }      
  }

?>