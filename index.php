<?php
  require('animal.php');
  require('Ape.php');
  require('Frog.php');


  $sheep = new Animal("shaun");

  echo "Nama hewan: $sheep->name <br/>"; // "shaun"
  echo "jumlah kaki dari hewan $sheep->name = $sheep->legs <br/>"; // 4
  echo "Memiliki darah dingin: $sheep->cold_blooded <br/>"; // "no"
  echo "<br/>";

  
//   index.php
  $kodok = new Frog("buduk");
  echo "Nama hewan: $kodok->name <br/>"; // "shaun"
  echo "jumlah kaki dari hewan $kodok->name = $kodok->legs <br/>"; // 4
  echo "Memiliki darah dingin: $kodok->cold_blooded <br/>"; // "no"
  echo "Jump: ";
  $kodok->jump(); // "hop hop"
  echo "<br/><br/>";

  $sungokong = new Ape("kera sakti");
  echo "Nama hewan: $sungokong->name <br/>"; // "shaun"
  echo "jumlah kaki dari hewan $sungokong->name = $sungokong->legs <br/>"; // 4
  echo "Memiliki darah dingin: $sungokong->cold_blooded <br/>"; // "no"
  echo "Yell: ";
  $sungokong->yell(); // "Auooo"
  echo "<br/><br/>";  

?>